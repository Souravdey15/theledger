import React from "react";
import Inventory from "../../components/inventory";
const InventoryContainer = (props) => {
  return <Inventory {...props} />;
};

export default InventoryContainer;
